import java.util.Scanner;

public class MainCircuitoEletronico {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CircuitoEletronico circuito = new CircuitoEletronico();

        System.out.println("Digite a tensão do circuito:");
        circuito.setTensao(scanner.nextDouble());

        System.out.println("Digite a corrente do circuito:");
        circuito.setCorrente(scanner.nextDouble());

        System.out.println("Resistência do circuito: " + circuito.calcularResistencia());
        System.out.println("Potência dissipada: " + circuito.calcularPotencia());
    }
}
