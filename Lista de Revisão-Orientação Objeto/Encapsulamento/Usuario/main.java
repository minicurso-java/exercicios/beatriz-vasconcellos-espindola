import java.util.Scanner;

public class MainUsuario {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Usuario usuario = new Usuario();

        int opcao;
        do {
            System.out.println("Escolha uma opção:");
            System.out.println("1. Definir nome");
            System.out.println("2. Definir email");
            System.out.println("3. Definir senha");
            System.out.println("4. Mostrar informações do usuário");
            System.out.println("0. Sair");
            opcao = scanner.nextInt();

            switch (opcao) {
                case 1:
                    System.out.println("Digite o nome do usuário:");
                    scanner.nextLine(); 
                    usuario.setNome(scanner.nextLine());
                    break;
                case 2:
                    System.out.println("Digite o email do usuário:");
                    scanner.nextLine()
                    usuario.setEmail(scanner.nextLine());
                    break;
                case 3:
                    System.out.println("Digite a senha do usuário:");
                    scanner.nextLine(); 
                    usuario.setSenha(scanner.nextLine());
                    break;
                case 4:
                    System.out.println("Nome: " + usuario.getNome());
                    System.out.println("Email: " + usuario.getEmail());
                    break;
                case 0:
                    System.out.println("Saindo...");
                    break;
                default:
                    System.out.println("Opção inválida.");
            }
        } while (opcao != 0);
    }
}
